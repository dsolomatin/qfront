## Icons

file to register every icon
../index.js and the icon should be store in ./components/icons/

# How to use a icon

```vue
<template lang="pug">
icon-base(icon-name='icon-popup' width='100' height='100' viewBox="0 0 18 18" iconColor='black')
    icon-star
</template>
```

```javascript
// Import the base components
import IconBase from '@/components/'

// Import the icon
import { IconStar } from '@/components/icons/'

```

## Option
- [icon-name] Icon name
- [width] Width
- [height] Height
- [viewBox] viewBox
- [iconColor] Icon - Color

# Looks like
![Icons](img/icons/icons.png)

<<<<<<< HEAD
# List
How to use
=======
- [X] icon-clock
> icon-base(icon-name='icon-clock' width='16' height='16' viewBox='0 0 16 16' iconColor='black')
  icon-clock

- [X] icon-delete
> icon-base(icon-name='icon-delete' width='34' height='34' viewBox='0 0 34 34' iconColor='#D94C4C')
  icon-delete

- [X] icon-earth 
> icon-base(icon-name='icon-earth' width='27' height='27' iconColor='black')
  icon-earth 

- [X] icon-google-play
> icon-base(icon-name='icon-google-play' width='165' height='55' iconColor='black')
  icon-google-play

- [X] [Not work as well]
> icon-base(icon-name='icon-letter' width='19' height='20' iconColor='black')
  icon-letter

- [X] Logo works ok
> icon-base(icon-name='icon-logo' width='200' height='200')
  icon-logo

- [X] Logo Hover works ok
> icon-base(icon-name='icon-logo-hover' width='200' height='200')
  icon-logo-hover

- [ ] Different element but similar lookup
> icon-base(icon-name='icon-man' width='40' height='40' viewBox='0 0 24 16' iconColor='black')
  icon-man

- [ ] [No tested]
> icon-base(icon-name='icon-write' width='200' height='200' iconColor='black')
  icon-write

- [X] Mark works ok [Should be call map_pin]
> icon-base(icon-name='icon-mark' width='13' height='18' viewBox='0 0 13 18' iconColor='black')
  icon-mark

- [X] icon-media
> icon-base(icon-name='icon-media' width='24' height='32' viewBox='0 0 24 32' iconColor='black')
  icon-media

- [X] icon-phone
> icon-base(icon-name='icon-phone' width='20' height='20' viewBox='0 0 20 20' iconColor='black')
  icon-phone

- [X] icon-qapsula
> icon-base(icon-name='icon-qapsula' width='73' height='52' viewBox='0 0 73 52')
  icon-qapsula

- [X] icon-question
> icon-base(icon-name='icon-question' width='13' height='20' viewBox='0 0 13 20' iconColor='#999999')
  icon-question

- [X] icon-rotation [should named reload]
> icon-base(icon-name='icon-rotation' width='32' height='30' viewBox='0 0 32 30' iconColor='black')
  icon-rotation

- [X] icon-sheet
> icon-base(icon-name='icon-sheet' width='400' height='400' viewBox='0 0 200 200' iconColor='black')
  icon-sheet 
>>>>>>> merge-branch

Width, height and viewBox

```
 b-row
  b-col
    ol
      li
        | icon-arrow-down
        br
        icon-base(icon-name='icon-arrow-down' width='30' height='18' viewbox='0 0 30 18')
          icon-arrow-down
      li
        | icon-email
        br
        icon-base(icon-name='icon-email' width="30" height="24" viewBox="0 0 30 24")
          icon-email
      li
        | icon-text ["T"]
        br
        icon-base(icon-name='icon-text' width='24' height='16')
          icon-text
      li
        | icon-letter-t
        br
        icon-base(icon-name='icon-letter-t' width='19' height='20' viewBox='0 0 19 20')
          icon-letter-t
      li
        | icon-facebook [only "F"]
        br
        icon-base(icon-name='icon-facebook' width='9' height='19')
          icon-facebook
      li
        | icon-instagram
        br
        icon-base(icon-name='icon-instagram' width='18' height='18')
          icon-instagram
      li
        | icon-ok
        br
        icon-base(icon-name='icon-ok' width='24' height='22')
          icon-ok
      li
        | icon-vk
        br
        icon-base(icon-name='icon-vk' width='24' height='14')
          icon-vk
      li
        | icon-youtube
        br
        icon-base(icon-name='icon-youtube' width='24' height='16')
          icon-youtube
      li
        | icon-app-store
        br
        icon-base(icon-name='icon-app-store' width='165', height='55', viewBox='0 0 165 55')
          icon-app-store
      li
        | icon-app-store-en
        br
        icon-base(icon-name='icon-app-store-en' width='165', height='55', viewBox='0 0 165 55')
          icon-app-store-en
      li
        | icon-bot
        br
        icon-base(icon-name='icon-bot' width='34', height='29', viewBox='0 0 34 29')
          icon-bot
      li
        | icon-subtract
        br
        icon-base(icon-name='icon-subtract' width="30" height="22" viewBox="0 0 30 22")
          icon-subtract
      li
        | icon-vkontacte
        br
        icon-base(icon-name='icon-vkontacte' width="30" height="22" viewBox="0 0 30 22")
          icon-vkontacte
  b-col
    ol
      li
        | icon-bubble
        br
        icon-base(icon-name='icon-bubble' width='28' height='28' viewBox="0 0 28 28")
          icon-bubble
      li
        | icon-cap
        br
        icon-base(icon-name='icon-cap' width='34' height='28' viewBox="0 0 34 28")
          icon-cap
      li
        | icon-clip
        br
        icon-base(icon-name='icon-clip' width='14' height='24' viewBox='0 0 14 24' iconColor='white' strokeFill='#2CB2DB', strokeWidth='3')
          icon-clip
      li
        | icon-clip-btn
        br
        icon-base(icon-name='icon-clip-btn' width="40" height="40" viewBox="0 0 40 40" iconColor='white' strokeFill='#2CB2DB', strokeWidth='3')
          icon-clip-btn
      li
        | icon-clock
        br
        icon-base(icon-name='icon-clock' width='16' height='16' viewBox='0 0 16 16')
          icon-clock
      li
        | icon-earth
        br
        icon-base(icon-name='icon-earth' width="27" height="27" viewBox="0 0 27 27")
          icon-earth
      li
        | icon-google-play
        br
        icon-base(icon-name='icon-google-play' width='165' height='55' viewBox='0 0 165 55')
          icon-google-play
      li
        | icon-google-play-en
        br
        icon-base(icon-name='icon-google-play-en' width='165' height='55' viewBox='0 0 165 55')
          icon-google-play-en
      li
        | icon-logo
        br
        icon-base(icon-name='icon-logo' width='141' height='42' viewBox='0 0 141 42')
          icon-logo
      li
        | icon-logo-hover
        br
        icon-base(icon-name='icon-logo-hover' width='141' height='42' viewBox='10 5 141 42')
          icon-logo-hover
      li
        | icon-box
        br
        icon-base(icon-name='icon-box' width='20' height='20' viewBox='0 0 20 20')
          icon-box
      li
        | icon-lent
        br
        icon-base(icon-name='icon-lent' width="24" height="24" viewBox="0 0 24 24")
          icon-lent
  b-col
    ol
      li
        | icon-man
        br
        icon-base(icon-name='icon-man' width='24' height='29' viewBox='0 0 24 29')
          icon-man
      li
        | icon-write
        br
        icon-base(icon-name='icon-write' width='24' height='29' viewBox='0 0 24 29')
          icon-write
      li
        | icon-avatar-btn
        br
        icon-base(icon-name='icon-avatar-btn' width="46" height="46" viewBox="0 0 46 46")
          icon-avatar-btn
      li
        | icon-mark
        br
        icon-base(icon-name='icon-mark' width='13' height='18' viewBox='0 0 13 18')
          icon-mark
      li
        | icon-media
        br
        icon-base(icon-name='icon-media' width='24' height='32' viewBox='0 0 24 32')
          icon-media
      li
        | icon-phone
        br
        icon-base(icon-name='icon-phone' width='20' height='20' viewBox='0 0 20 20')
          icon-phone
      li
        | icon-qapsula
        br
        icon-base(icon-name='icon-qapsula' width='73' height='52' viewBox='0 0 73 52')
          icon-qapsula
      li
        | icon-question
        br
        icon-base(icon-name='icon-question' width='13' height='20' viewBox='0 0 13 20')
          icon-question
      li
        | icon-rotation
        br
        icon-base(icon-name='icon-rotation' width='32' height='30' viewBox='0 0 32 30')
          icon-rotation
      li
        | icon-sheet
        br
        icon-base(icon-name='icon-sheet' width="22" height="30" viewBox="0 0 22 30")
          icon-sheet
      li
        | icon-star
        br
        icon-base(icon-name='icon-star' width='100' height='100' viewBox="0 0 18 18")
          icon-star
  b-col
    ol
      li
        | icon-facebook-box
        br
        icon-base(icon-name='icon-facebook-box' width='46' height='46' viewBox="0 0 46 46")
          icon-facebook-box
      li
        | icon-ok-box
        br
        icon-base(icon-name='icon-ok-box' width='46' height='46' viewBox="0 0 46 46")
          icon-ok-box
      li
        | icon-vk-box
        br
        icon-base(icon-name='icon-vk-box' width='46' height='46' viewBox="0 0 46 46")
          icon-vk-box
      li
        | icon-qr
        br
        icon-base(icon-name='icon-qr' width="23" height="27" viewBox="0 0 23 27")
          icon-qr
      li
        | icon-burguer-menu
        br
        icon-base(icon-name='burguermenu' width="22" height="18" viewBox="0 0 22 18" )
          icon-burguer-menu
      li
        | icon-x
        br
        icon-base(icon-name='icon-x' width='10' height='10' viewBox='0 0 10 10')
          icon-x
      li
        | icon-x-l
        br
        icon-base(icon-name='icon-x-l' width='20' height='20' viewBox='0 0 20 20')
          icon-x-l
      li
        | icon-man-siluete
        br
        icon-base(icon-name='icon-man-siluete' width='17' height='20' viewbox='0 0 17 20')
          icon-man-siluete
      li
        | icon-suitcase
        br
        icon-base(icon-name='icon-suitcase' width="18" height="15" viewBox="0 0 18 15")
          icon-suitcase
      li
        | icon-send-airplane
        br
        icon-base(icon-name='icon-send-airplane' width="18" height="18" viewBox="0 0 18 18")
          icon-send-airplane
      li
        | icon-send-air-btn
        br
        icon-base(icon-name='icon-send-air-btn' width="40" height="40" viewBox="0 0 40 40" iconColor='#2CB2DB')
          icon-send-air-btn
  b-col
    ol
      li
        | icon-load-img
        br
        icon-base(icon-name='icon-load-img' width="22" height="23" viewBox="0 0 22 23")
          icon-load-img
      li
        | icon-circular-check
        br
        icon-base(icon-name='icon-circular-check' width='16' height='16' viewbox='0 0 16 16')
          icon-circular-check
      li
        | icon-key
        br
        icon-base(icon-name='icon-key' width="27" height="16" viewBox="0 0 27 16")
          icon-key
      li
        | icon-img
        br
        icon-base(icon-name='icon-img' width="18" height="18" viewBox="0 0 18 18")
          icon-img
      li
        | icon-scann-doc
        br
        icon-base(icon-name='icon-scann-doc' width="18" height="18" viewBox="0 0 18 18")
          icon-scann-doc
      li
        | icon-scann
        br
        icon-base(icon-name='icon-scann' width="18" height="18" viewBox="0 0 18 18")
          icon-scann
      li
        | icon-scann-zip
        br
        icon-base(icon-name='icon-scann-zip' width="18" height="18" viewBox="0 0 18 18")
          icon-scann-zip
      li
        | icon-x-red
        br
        icon-base(icon-name='icon-x-red' width="24" height="24" viewBox="0 0 24 24" iconColor='#D94C4C')
          icon-x-red
      li
        | icon-forward-btn
        br
        icon-base(icon-name='icon-forward-btn' width="40" height="40" viewBox="0 0 40 40" iconColor="white")
          icon-forward-btn
      li
        | icon-like
        br
        icon-base(icon-name='icon-like' width="21" height="21" viewBox="0 0 21 21")
          icon-like
      li
        | icon-star-siluete
        br
        icon-base(icon-name='icon-star-siluete' width="21" height="21" viewBox="0 0 21 21")
          icon-star-siluete
      li
        | icon-share
        br
        icon-base(icon-name='icon-share' width="24" height="20" viewBox="0 0 24 20")
          icon-share
      li
        | icon-moon
        br
        icon-base(icon-name='icon-moon' width="19" height="19" viewBox="0 0 19 19")
          icon-moon
      li
        | icon-filter
        br
        icon-base(icon-name='icon-filter' width="12" height="12" viewBox="0 0 12 12")
          icon-filter
      li
        | icon-eye
        br
        icon-base(icon-name='icon-eye' width="24" height="14" viewbox='0 0 24 14')
          icon-eye
      li
        | icon-eye-blind
        br
        icon-base(icon-name='icon-eye-blind' width='25' height='16' viewbox='0 0 25 16')
          icon-eye-blind
  b-col
    ol
      li
        | icon-other-topics
        br
        .other-style
          icon-base(icon-name='icon-other-topics' width="25" height="25" viewBox="0 0 25 25" iconColor="white")
            icon-other-topics
      li
        | icon-cardiology
        br
        icon-base(icon-name='icon-cardiology' width="66" height="66" viewBox="0 0 66 66")
          icon-cardiology
      li
        | icon-traumatology
        br
        icon-base(icon-name='icon-traumatology' width='16' height='36' viewbox='0 0 16 36')
          icon-traumatology
      li
        | icon-therapy
        br
        icon-base(icon-name='icon-therapy' width='23' height='34' viewbox='0 0 23 34')
          icon-therapy
      li
        | icon-orthopedics
        br
        icon-base(icon-name='icon-orthopedics' width="18" height="36" viewBox="0 0 18 36")
          icon-orthopedics
      li
        | icon-neurology
        br
        icon-base(icon-name='icon-neurology' width="30" height="26" viewBox="0 0 30 26")
          icon-neurology
      li
        | icon-female
        br
        icon-base(icon-name='icon-female' width="18" height="31" viewBox="0 0 18 31")
          icon-female
      li
        | icon-dermatology
        br
        icon-base(icon-name='icon-dermatology' width='25' height='29' viewbox='0 0 25 29')
          icon-dermatology
      li
        | icon-urology
        br
        icon-base(icon-name='icon-urology' width="32" height="26" viewBox="0 0 32 26")
          icon-urology
      li
        | icon-chat-prop
        br
        icon-base(icon-name='icon-chat-prop' width="29" height="24" viewBox="0 0 29 24")
          icon-chat-prop
      li
        | icon-link
        br
        icon-base(icon-name='icon-link' width="19" height="18" viewBox="0 0 19 18")
          icon-link

```

# TODO

- [X] Put all Icons
- [X] Reemplace on the project
- [X] Put the correct name
- [X] Put everything on the correct folder
- [X] Resize and change color icon
- [ ] Create a component to join all icons and change frame and shadow