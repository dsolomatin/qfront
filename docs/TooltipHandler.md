# This is a Tooltip Custom component documentation

## use

-- Import to use
```javascript
import TooltipHandler from '@/components/TooltipHandler.vue'
export default {
  components: {
    TooltipHandler
  }
}
```

-- Without use html injection. Simple tooltip with Qapsula style

```pug
b-button#tooltip-question
  tooltip-handler(idTrigger="tooltip-question", trigger="click hover focus", textPromp="Код введён неверно" :htmlShow="false")
```

### Options:
- *idTrigger* [Id trigger component] e.g. b-button#tooltip-question
- *trigger* [Trigger] Same b-tooltip (Vue-boostrap)
- *textPromp* [Text to be prompted]
- *htmlShow* [Bool] If the text content have a html content (default: false)
- *position* [location arrow] top, left, right... (default: left)
- *colorText* [Color text] (default: #D94C4C 'red')

### Looks like

![Tooltip](img/tooltip/tooltipstyle.png)

- With use html injection.

```pug
b-button#tooltip-question
  tooltip-handler(idTrigger="tooltip-question", trigger="click hover focus", textPromp="Код введён неверно" :htmlShow="true")
    div(slot="htmlTootip")
      | &nbsp; Some text
      a(href="/") Some text
      | &nbsp; Some text
```

Note:

- :htmlShow [true]
- add a div with slot

### Looks like

![Tooltiphtml](img/tooltip/tooltiphtml.png)
