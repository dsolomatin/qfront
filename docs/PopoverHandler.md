# This is a Popover Custom component documentation

## use

-- Import to use
```javascript
import PopoverHandler from '@/components/PopoverHandler.vue'
export default {
  components: {
    PopoverHandler
  }
}
```

-- Without 

```pug
b-button#idTriggerComponent Btn
  popover-handler(idTrigger="idTriggerComponent" titlePop="Пароль слишком простой" contextPop="Замените одну или две строчные буквы прописными и добавьте цифры")
```

### Options:
- *idTrigger* [Id trigger component] e.g. b-button#tooltip-question
- *trigger* [Trigger] Same b-tooltip (Vue-boostrap)
- *titlePop* [Title to be prompted]
- *contentPop [Content pop]
- *position* [location arrow] top, left, right... (default: left)
- *colorText* [Color text] (default: #D94C4C 'red')
- *barProgress* [Boolean Show bar progress] (default: true)
- *colorBarProgress* [Color bar] (default: warning)
- *valueBarProgess* [Value] (default: 0)

### Looks like

![Popover](img/popover/popover.png)
