import Vue from 'vue'
import Router from 'vue-router'
import Header from '@/layouts/Header.vue'
import Footer from '@/layouts/Footer.vue'
import Slick from 'vue-slick'

import {
  Ask,
  Feedback,
  Doctors,
  Question,
  Language,
  Social
} from '@/components/'

import { AboutPage,
  AgreementPage,
  AskPage,
  BasePage,
  ConfidentialPage,
  ContactsPage,
  DevelopsPage,
  FaqPage,
  FeedbackPage,
  HomePage,
  PatientsPage,
  DoctorsPage,
  Login,
  AccountManager,
  RestrictedAccess,
  NoMatchPage
} from '@/pages'

// import { i18n } from '../setup/i18n-setup'
import { i18n, loadLanguageAsync } from '@/setup/i18n-setup'

Vue.component('app-ask', Ask)
Vue.component('app-doctors', Doctors)
Vue.component('app-feedback', Feedback)
Vue.component('app-footer', Footer)
Vue.component('app-header', Header)
Vue.component('app-language', Language)
Vue.component('app-social', Social)
Vue.component('app-slick', Slick)
Vue.component('app-social', Social)
Vue.component('app-question', Question)

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: `/${i18n.locale}`
    },
    {
      path: '/:lang',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '/',
          name: 'home',
          component: HomePage
        },
        {
          path: 'base',
          name: 'base',
          component: BasePage,
          props: true
        },
        {
          path: 'about',
          name: 'about',
          component: AboutPage,
          props: true
        },
        {
          path: 'agreement',
          name: 'agreement',
          component: AgreementPage,
          props: true
        },
        {
          path: 'patients',
          name: 'patients',
          component: PatientsPage,
          props: true
        },
        {
          path: 'ask',
          name: 'ask',
          component: AskPage,
          props: true
        },
        {
          path: 'confidential',
          name: 'confidential',
          component: ConfidentialPage,
          props: true
        },
        {
          path: 'doctors',
          name: 'doctors',
          component: DoctorsPage,
          props: true
        },
        {
          path: 'contacts',
          name: 'contacts',
          component: ContactsPage,
          props: true
        },
        {
          path: 'develops',
          name: 'develops',
          component: DevelopsPage,
          meta: {
            auth: true
          }, // this is for login, in this moment is a test
          props: true
        },
        {
          path: 'login',
          name: 'login',
          component: Login,
          props: true
        },
        {
          path: 'users/account',
          name: 'account-manager',
          component: AccountManager,
          props: true
        },
        {
          path: 'restrictedaccess',
          name: 'restricted-access',
          component: RestrictedAccess,
          props: true
        },
        {
          path: 'faq',
          name: 'faq',
          component: FaqPage,
          props: true
        },
        {
          path: 'feedback',
          name: 'feedback',
          component: FeedbackPage,
          props: true
        },
        {
          path: '/*',
          name: '404',
          component: NoMatchPage,
          props: true
        }
      ]
    }
  ]
})

// This code should be here till fix routing problems

// // use beforeEach route guard to set the language
// router.beforeEach((to, from, next) => {
//   // use the language from the routing param or default language
//   let language = to.params.lang
//   if (!language) {
//     language = 'en'
//   }

//   // set the current language for i18n.
//   i18n.locale = language
//   next()
// })

router.beforeEach((to, from, next) => {
  let user = ''
  let auth = to.matched.some(record => record.meta.auth)

  console.log(user, auth)
  // Load the language
  const lang = to.params.lang
  loadLanguageAsync(lang).then(() => {
    // next()
    // Validate if the user is authenticated!
    if (auth && !user) next('login')
    else if (!auth && user) next('/')
    else next()
  })
})

export default router
