import configService from './config'

const fetchService = {}

fetchService.getDoctorsSlider = function (q) {
  return fetch(configService.apiUrl + q).then(data => data.json())
}

fetchService.getLanguage = function (q) {
  return fetch(configService.apiUrl + 'languages/' + q).then(data => data.json())
}

export default fetchService
