export const LANGUAGES = {
  'ab': {
    '404NoFound': 'jjj',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': '',
    'cosntructPage': {
      'content': '',
      'title': ''
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': '',
      'accademyResearchTitle': '',
      'askDtr': '',
      'btnBlue': '',
      'capabilitiesTitle': '',
      'cashBackText': '',
      'cashBackTitle': '',
      'chat': {
        'btnSend': '',
        'textArea': ''
      },
      'chatBotText': '',
      'chatBotTitle': '',
      'company': '',
      'confidentiality': '',
      'consultationText': '',
      'consultationTitle': '',
      'contact': '',
      'deabetes': {
        'check': '',
        'from': '',
        'title': ''
      },
      'doCosult': '',
      'doctorsBestTitle': '',
      'headerNavLinkAbout': '',
      'headerNavLinkBase': '',
      'headerNavLinkDoctors': '',
      'headerNavLinkPatients': '',
      'loginRegistrationBtn': '',
      'mediaText': '',
      'mediaTitle': '',
      'medicalRecordText': '',
      'medicalRecordTitle': '',
      'opportunities': '',
      'questionFormLeft': '',
      'questionFormSubtitle': '',
      'questionFormTitle': '',
      'questionsText': '',
      'questionsTitle': '',
      'termsAndUse': ''
    },
    'nameCompany': '',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  },
  'br': {
    '404NoFound': '404 não encontrado',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': 'Todos os direitos reservados',
    'cosntructPage': {
      'content': 'volte mais tarde.',
      'title': 'Página em construção,'
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': 'Respostas e artigos de especialistas para suas perguntas.',
      'accademyResearchTitle': 'Conteúdo acadêmico',
      'askDtr': 'Consulte nossos médicos',
      'btnBlue': 'Saiba mais!',
      'capabilitiesTitle': 'Recursos do Qapsula',
      'cashBackText': 'Digitalize prescrições de farmácias, clínicas, laboratórios e ganhe pontos',
      'cashBackTitle': 'Dinheiro de volta',
      'chat': {
        'btnSend': 'Enviar',
        'textArea': 'Descreva seus sintomas'
      },
      'chatBotText': 'Medicamentos e lembretes de medicamentos',
      'chatBotTitle': 'Chat Bot',
      'company': 'Companhia',
      'confidentiality': 'Termos de Confidencialidade',
      'consultationText': 'O Qapsula informará quando e onde está sua consulta, no momento certo. Disponível na Web, iOS e Android',
      'consultationTitle': 'Qapsula permite-nos consultar os nossos médicos gratuitamente',
      'contact': 'Contato',
      'deabetes': {
        'check': 'Verificar',
        'from': 'de',
        'title': 'Controle de diabetes'
      },
      'doCosult': 'Faça uma consulta',
      'doctorsBestTitle': 'Os melhores médicos ao seu serviço',
      'headerNavLinkAbout': 'Sobre nós',
      'headerNavLinkBase': 'Conhecimento básico',
      'headerNavLinkDoctors': 'Médicos',
      'headerNavLinkPatients': 'Pacientes',
      'loginRegistrationBtn': 'Login | Inscreva-se',
      'mediaText': 'Entre em contato com o médico a qualquer hora, em qualquer lugar',
      'mediaTitle': 'Áudio e Vídeo',
      'medicalRecordText': 'Armazenamento seguro de dados médicos. Capacidade de carregar ECG, MRI, CT',
      'medicalRecordTitle': 'História clínica',
      'opportunities': 'Trabalhar com a gente',
      'questionFormLeft': 'Ele tem 150 caracteres restantes ...',
      'questionFormSubtitle': 'Sua pergunta será enviada a todos os especialistas neste campo.',
      'questionFormTitle': 'Consulte mais de 10 mil médicos',
      'questionsText': 'Consulte o médico gratuitamente 24 horas',
      'questionsTitle': 'Consultas gratuitas',
      'termsAndUse': 'Termos de uso'
    },
    'nameCompany': 'Cápsula',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  },
  'en': {
    '404NoFound': '404 not found',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': 'All rights reserved',
    'cosntructPage': {
      'content': 'check back later.',
      'title': 'The page is under construction'
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': 'Answers and articles of specialists to your questions',
      'accademyResearchTitle': 'Accademic Content',
      'askDtr': 'Ask to the doctor',
      'btnBlue': 'I want to know more!',
      'capabilitiesTitle': 'Qapsula features',
      'cashBackText': 'Scan checks from pharmacies, clinics, laboratories and get points',
      'cashBackTitle': 'Cash back',
      'chat': {
        'btnSend': 'Submit',
        'textArea': 'Describe your symptoms...'
      },
      'chatBotText': 'Reminders for taking medication. Appointment to the doctor',
      'chatBotTitle': 'Chat Bot',
      'company': 'Company',
      'confidentiality': 'Confidentiality terms',
      'consultationText': 'Qapsula will tell you what and when to do, hold a consultation with a specialist and write down for a study. Available on Web, iOS and Android',
      'consultationTitle': 'Qapsula allows you to get an online doctor consultation for free!',
      'contact': 'Contact',
      'deabetes': {
        'check': 'Check',
        'from': 'from',
        'title': 'Deabetes control'
      },
      'doCosult': 'Do a consult',
      'doctorsBestTitle': 'The best doctors of our service',
      'headerNavLinkAbout': 'About Us',
      'headerNavLinkBase': 'Knowledge base',
      'headerNavLinkDoctors': 'Doctors',
      'headerNavLinkPatients': 'Patients',
      'loginRegistrationBtn': 'Login | Register',
      'mediaText': 'The ability to communicate with a doctor from any device and from anywhere in the world',
      'mediaTitle': 'Audio and Video',
      'medicalRecordText': 'Safe storage of medical data. Ability to load ECG, MRI, CT',
      'medicalRecordTitle': 'Medical Record',
      'opportunities': 'Opportunities and vacancies',
      'questionFormLeft': '150 characters left ...',
      'questionFormSubtitle': 'Your question will be sent to all specialists in this field.',
      'questionFormTitle': 'Ask anything from 10,000 of our doctors',
      'questionsText': 'Ask your doctor a question online for free. Reply within 24 hours',
      'questionsTitle': 'Free questions',
      'termsAndUse': 'Terms of Use'
    },
    'nameCompany': 'Capsula',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  },
  'es': {
    '404NoFound': '404 Página no encontrada',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': 'Todos los derechos reservados',
    'cosntructPage': {
      'content': 'regresa después.',
      'title': 'Página en construcción,'
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': 'Respuestas y artículos de especialistas a sus preguntas.',
      'accademyResearchTitle': 'Contenido académico',
      'askDtr': 'Consulta a nuestros doctores',
      'btnBlue': 'Conoce más!',
      'capabilitiesTitle': 'Características de Qapsula',
      'cashBackText': 'Escanee recetas de farmacias, clínicas, laboratorios y obtenga puntos',
      'cashBackTitle': 'Cash back',
      'chat': {
        'btnSend': 'Enviar',
        'textArea': 'Describa sus síntomas'
      },
      'chatBotText': 'Recordatorios de medicamentos y medicinas',
      'chatBotTitle': 'Chat Bot',
      'company': 'Compañia',
      'confidentiality': 'Términos de confidencialidad',
      'consultationText': 'Qapsula le dirá cuando y donde es su consulta, justo en el momento necesario. Disponible en Web, iOS y Android',
      'consultationTitle': 'Qapsula permite hacer consultas a nuestros doctores de manera gratuita',
      'contact': 'Contacto',
      'deabetes': {
        'check': 'Check',
        'from': 'desde',
        'title': 'Control de diabetes'
      },
      'doCosult': 'Realiza una consulta',
      'doctorsBestTitle': 'Los mejores doctores a su servicio',
      'headerNavLinkAbout': 'Acerca de nosotros',
      'headerNavLinkBase': 'Conocimiento base',
      'headerNavLinkDoctors': 'Doctores',
      'headerNavLinkPatients': 'Pacientes',
      'loginRegistrationBtn': 'Login | Registrarse',
      'mediaText': 'Comuníquese con el doctor en cualquier momento y en cualquier lugar',
      'mediaTitle': 'Audio y Vídeo',
      'medicalRecordText': 'Almacenamiento seguro de datos médicos. Capacidad para cargar ECG, MRI, CT',
      'medicalRecordTitle': 'Historial clínico',
      'opportunities': 'Trabaja con nosotros',
      'questionFormLeft': 'Le quedan 150 caracteres...',
      'questionFormSubtitle': 'Su consulta será enviada a todos los especialistas',
      'questionFormTitle': 'Consulta a más de 10 mil doctores',
      'questionsText': 'Consulte al Doctor gratuitamente las 24 horas',
      'questionsTitle': 'Consultas gratuitas',
      'termsAndUse': 'Términos de uso'
    },
    'nameCompany': 'Cápsula',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  },
  'fr': {
    '404NoFound': '404 non trouvé',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': 'Tous les droits sont réservés',
    'cosntructPage': {
      'content': '',
      'title': ''
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': 'Réponses et articles de spécialistes à vos questions.',
      'accademyResearchTitle': 'Contenu académique',
      'askDtr': '',
      'btnBlue': 'En savoir plus!',
      'capabilitiesTitle': 'Qapsula caractéristiques',
      'cashBackTitle': 'Cash back',
      'chat': {
        'btnSend': '',
        'textArea': ''
      },
      'chatBotText': '',
      'chatBotTitle': '',
      'company': '',
      'confidentiality': '',
      'consultationText': 'Qapsula vous dira quand et où se trouve votre requête, au bon moment. Disponible sur le Web, iOS et Android',
      'consultationTitle': 'Qapsula nous permet de consulter gratuitement nos médecins',
      'contact': '',
      'deabetes': {
        'check': '',
        'from': '',
        'title': ''
      },
      'doCosult': '',
      'doctorsBestTitle': 'Les meilleurs docteurs à votre service',
      'headerNavLinkAbout': '',
      'headerNavLinkBase': '',
      'headerNavLinkDoctors': '',
      'headerNavLinkPatients': '',
      'loginRegistrationBtn': '',
      'mediaText': '',
      'mediaTitle': '',
      'medicalRecordText': 'Stockage sécurisé des données médicales. Capacité à charger ECG, IRM, CT',
      'medicalRecordTitle': 'Histoire clinique',
      'opportunities': '',
      'questionFormLeft': '150 caractères restants...',
      'questionFormSubtitle': 'Votre question sera envoyée à tous les spécialistes dans ce domaine.',
      'questionFormTitle': 'Consulter plus de 10 mille médecins',
      'questionsText': '',
      'questionsTitle': '',
      'termsAndUse': ''
    },
    'nameCompany': 'Capsule',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  },
  'gr': {
    '404NoFound': '404 Nicht gefunden',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': 'Alle Rechte vorbehalten',
    'cosntructPage': {
      'content': '',
      'title': ''
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': '',
      'accademyResearchTitle': '',
      'askDtr': '',
      'btnBlue': '',
      'capabilitiesTitle': '',
      'cashBackText': '',
      'cashBackTitle': '',
      'chat': {
        'btnSend': '',
        'textArea': ''
      },
      'chatBotText': '',
      'chatBotTitle': '',
      'company': '',
      'confidentiality': '',
      'consultationText': '',
      'consultationTitle': '',
      'contact': '',
      'deabetes': {
        'check': '',
        'from': '',
        'title': ''
      },
      'doCosult': '',
      'doctorsBestTitle': '',
      'headerNavLinkAbout': '',
      'headerNavLinkBase': '',
      'headerNavLinkDoctors': '',
      'headerNavLinkPatients': '',
      'loginRegistrationBtn': '',
      'mediaText': '',
      'mediaTitle': '',
      'medicalRecordText': '',
      'medicalRecordTitle': '',
      'opportunities': '',
      'questionFormLeft': '150 Zeichen übrig ...',
      'questionFormSubtitle': 'Ihre Frage wird an alle Spezialisten in diesem Bereich gesendet.',
      'questionFormTitle': 'Konsultieren Sie mehr als 10.000 Ärzte',
      'questionsText': '',
      'questionsTitle': '',
      'termsAndUse': ''
    },
    'nameCompany': 'Kapsel',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  },
  'id': {
    '404NoFound': '404 tidak ditemukan',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': 'Seluruh hak cipta',
    'cosntructPage': {
      'content': '',
      'title': ''
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': '',
      'accademyResearchTitle': '',
      'askDtr': '',
      'btnBlue': '',
      'capabilitiesTitle': '',
      'cashBackText': '',
      'cashBackTitle': '',
      'chat': {
        'btnSend': '',
        'textArea': ''
      },
      'chatBotText': '',
      'chatBotTitle': '',
      'company': '',
      'confidentiality': '',
      'consultationText': '',
      'consultationTitle': '',
      'contact': '',
      'deabetes': {
        'check': '',
        'from': '',
        'title': ''
      },
      'doCosult': '',
      'doctorsBestTitle': '',
      'headerNavLinkAbout': '',
      'headerNavLinkBase': '',
      'headerNavLinkDoctors': '',
      'headerNavLinkPatients': '',
      'loginRegistrationBtn': '',
      'mediaText': '',
      'mediaTitle': '',
      'medicalRecordText': '',
      'medicalRecordTitle': '',
      'opportunities': '',
      'questionFormLeft': '150 karakter tersisa ...',
      'questionFormSubtitle': 'Pertanyaan Anda akan dikirim ke semua spesialis di bidang ini.',
      'questionFormTitle': 'Konsultasikan dengan lebih dari 10 ribu dokter',
      'questionsText': '',
      'questionsTitle': '',
      'termsAndUse': ''
    },
    'nameCompany': 'Kapsul',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  },
  'ru': {
    '404NoFound': '404 Страница не найдена',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': 'Все права защищены',
    'cosntructPage': {
      'content': 'загляните позже.',
      'title': 'Страница находится в разработке'
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': 'Ответы и статьи специалистов по интересующим вас вопросам',
      'accademyResearchTitle': 'База знаний',
      'askDtr': 'Задать вопрос врачу',
      'btnBlue': 'Хочу знать больше!',
      'capabilitiesTitle': 'Возможности Qapsula',
      'cashBackText': 'Сканируйте чеки из аптек, клиник, лабораторий и получайте баллы',
      'cashBackTitle': 'Cash back',
      'chat': {
        'btnSend': 'Отправить',
        'textArea': 'Опишите ваши симптомы…'
      },
      'chatBotText': 'Напоминания о приёме лекарств. Запись на приём к врачу',
      'chatBotTitle': 'Qapsula бот',
      'company': 'Компания',
      'confidentiality': 'Конфиденциальность',
      'consultationText': 'Qapsula подскажет, что и когда делать, проведёт консультацию со специалистом и запишет на исследование. Доступно в Web, на iOS и Android',
      'consultationTitle': 'Qapsula позволяет получить онлайн-консультацию врача бесплатно!',
      'contact': 'Контакты',
      'deabetes': {
        'check': 'Акку-Чек',
        'from': 'с',
        'title': 'Контроль диабета'
      },
      'doCosult': 'Стать консультантом',
      'doctorsBestTitle': 'Лучшие врачи нашего сервиса',
      'headerNavLinkAbout': 'О нас',
      'headerNavLinkBase': 'База знаний',
      'headerNavLinkDoctors': 'Врачам',
      'headerNavLinkPatients': 'Пациентам',
      'loginRegistrationBtn': 'Вход | Регистрация',
      'mediaText': 'Возможность общения с врачом с любого устройства и из любой точки мира',
      'mediaTitle': 'Аудио и Видео',
      'medicalRecordText': 'Безопасное хранение медицинских данных. Возможность загружать ЭКГ, МРТ, КТ',
      'medicalRecordTitle': 'Медицинская карта',
      'opportunities': 'Возможности',
      'questionFormLeft': 'Осталось 150 символов...',
      'questionFormSubtitle': 'Ваш вопрос будет отправлен всем специалистам в данной области',
      'questionFormTitle': 'Спросите что угодно у 10000 наших врачей',
      'questionsText': 'Задайте вопрос врачу онлайн бесплатно. Ответ в течение 24 часов',
      'questionsTitle': 'Бесплатные вопросы ',
      'termsAndUse': 'Условия использования'
    },
    'nameCompany': 'Капсула',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  },
  'tr': {
    '404NoFound': '404 sıra ditemukan',
    'aboutPage': {
      'content': '',
      'title': ''
    },
    'allRightsReserved': 'Her hakkı saklıdır',
    'cosntructPage': {
      'content': '',
      'title': ''
    },
    'doctorsPage': {
      'content': '',
      'title': ''
    },
    'home': {
      'accademyResearchText': '',
      'accademyResearchTitle': '',
      'askDtr': '',
      'btnBlue': '',
      'capabilitiesTitle': '',
      'cashBackText': '',
      'cashBackTitle': '',
      'chat': {
        'btnSend': '',
        'textArea': ''
      },
      'chatBotText': '',
      'chatBotTitle': '',
      'company': '',
      'confidentiality': '',
      'consultationText': '',
      'consultationTitle': '',
      'contact': '',
      'deabetes': {
        'check': '',
        'from': '',
        'title': ''
      },
      'doCosult': '',
      'doctorsBestTitle': '',
      'headerNavLinkAbout': '',
      'headerNavLinkBase': '',
      'headerNavLinkDoctors': '',
      'headerNavLinkPatients': '',
      'loginRegistrationBtn': '',
      'mediaText': '',
      'mediaTitle': '',
      'medicalRecordText': '',
      'medicalRecordTitle': '',
      'opportunities': '',
      'questionFormLeft': '150 karakter kaldı...',
      'questionFormSubtitle': 'Sorunuz bu alandaki tüm uzmanlara gönderilecektir.',
      'questionFormTitle': '10 binden fazla doktora danışın',
      'questionsText': '',
      'questionsTitle': '',
      'termsAndUse': ''
    },
    'nameCompany': 'kapsül',
    'patientsPage': {
      'content': '',
      'title': ''
    }
  }
}
