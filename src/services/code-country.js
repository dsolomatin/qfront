// https://github.com/EducationLink/vue-tel-input
const allCountries = [
  [
    'Afghanistan (‫افغانستان‬‎)',
    'Афганистан',
    'af',
    '93'
  ],
  [
    'Albania (Shqipëri)',
    'Албания',
    'al',
    '355'
  ],
  [
    'Algeria (‫الجزائر‬‎)',
    'Алжир',
    'dz',
    '213'
  ],
  [
    'American Samoa',
    'Американское Самоа',
    'as',
    '1684'
  ],
  [
    'Andorra',
    'Андорра',
    'ad',
    '376'
  ],
  [
    'Angola',
    'Ангола',
    'ao',
    '244'
  ],
  [
    'Anguilla',
    'Ангилья',
    'ai',
    '1264'
  ],
  [
    'Antigua and Barbuda',
    'Антигуа и Барбуда',
    'ag',
    '1268'
  ],
  [
    'Argentina',
    'Аргентина',
    'ar',
    '54'
  ],
  [
    'Armenia (Հայաստան)',
    'Армения',
    'am',
    '374'
  ],
  [
    'Aruba',
    'Аруба',
    'aw',
    '297'
  ],
  [
    'Australia',
    'Австралия',
    'au',
    '61',
    0
  ],
  [
    'Austria (Österreich)',
    'Австрия',
    'at',
    '43'
  ],
  [
    'Azerbaijan (Azərbaycan)',
    'Азербайджан',
    'az',
    '994'
  ],
  [
    'Bahamas',
    'Багамы',
    'bs',
    '1242'
  ],
  [
    'Bahrain (‫البحرين‬‎)',
    'Бахрейн',
    'bh',
    '973'
  ],
  [
    'Bangladesh (বাংলাদেশ)',
    'Бангладеш',
    'bd',
    '880'
  ],
  [
    'Barbados',
    'Барбадос',
    'bb',
    '1246'
  ],
  [
    'Belarus (Беларусь)',
    'Беларусь',
    'by',
    '375'
  ],
  [
    'Belgium (België)',
    'Бельгия',
    'be',
    '32'
  ],
  [
    'Belize',
    'Белиз',
    'bz',
    '501'
  ],
  [
    'Benin (Bénin)',
    'Бенин',
    'bj',
    '229'
  ],
  [
    'Bermuda',
    'Бермуды',
    'bm',
    '1441'
  ],
  [
    'Bhutan (འབྲུག)',
    'Бутан',
    'bt',
    '975'
  ],
  [
    'Bolivia',
    'Боливия',
    'bo',
    '591'
  ],
  [
    'Bosnia and Herzegovina (Босна и Херцеговина)',
    'Босния и Герцеговина',
    'ba',
    '387'
  ],
  [
    'Botswana',
    'Ботсвана',
    'bw',
    '267'
  ],
  [
    'Brazil (Brasil)',
    'Бразилия',
    'br',
    '55'
  ],
  [
    'British Indian Ocean Territory',
    'Британская Территория В Индийском Океане',
    'io',
    '246'
  ],
  [
    'British Virgin Islands',
    'Британские Виргинские Острова',
    'vg',
    '1284'
  ],
  [
    'Brunei',
    'Бруней',
    'bn',
    '673'
  ],
  [
    'Bulgaria (България)',
    'Болгария',
    'bg',
    '359'
  ],
  [
    'Burkina Faso',
    'Буркина-Фасо',
    'bf',
    '226'
  ],
  [
    'Burundi (Uburundi)',
    'Бурунди',
    'bi',
    '257'
  ],
  [
    'Cambodia (កម្ពុជា)',
    'Камбоджа',
    'kh',
    '855'
  ],
  [
    'Cameroon (Cameroun)',
    'Камерун',
    'cm',
    '237'
  ],
  [
    'Canada',
    'Канада',
    'ca',
    '1',
    1,
    ['204', '226', '236', '249', '250', '289', '306', '343', '365', '387', '403', '416', '418', '431', '437', '438', '450', '506', '514', '519', '548', '579', '581', '587', '604', '613', '639', '647', '672', '705', '709', '742', '778', '780', '782', '807', '819', '825', '867', '873', '902', '905']
  ],
  [
    'Cape Verde (Kabu Verdi)',
    'Кабо-Верде',
    'cv',
    '238'
  ],
  [
    'Caribbean Netherlands',
    'Карибские Нидерланды',
    'bq',
    '599',
    1
  ],
  [
    'Cayman Islands',
    'Каймановы Острова',
    'ky',
    '1345'
  ],
  [
    'Central African Republic (République centrafricaine)',
    'Центральноафриканская Республика',
    'cf',
    '236'
  ],
  [
    'Chad (Tchad)',
    'Чад',
    'td',
    '235'
  ],
  [
    'Chile',
    'Чили',
    'cl',
    '56'
  ],
  [
    'China (中国)',
    'Китай',
    'cn',
    '86'
  ],
  [
    'Christmas Island',
    'Остров Рождества',
    'cx',
    '61',
    2
  ],
  [
    'Cocos (Keeling) Islands',
    'Кокосовые (Килинг) Островов',
    'cc',
    '61',
    1
  ],
  [
    'Colombia',
    'Колумбия',
    'co',
    '57'
  ],
  [
    'Comoros (‫جزر القمر‬‎)',
    'Коморские острова',
    'km',
    '269'
  ],
  [
    'Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)',
    'Конго (ДРК)',
    'cd',
    '243'
  ],
  [
    'Congo (Republic) (Congo-Brazzaville)',
    'Конго (Республика)',
    'cg',
    '242'
  ],
  [
    'Cook Islands',
    'Острова Кука',
    'ck',
    '682'
  ],
  [
    'Costa Rica',
    'Коста-Рика',
    'cr',
    '506'
  ],
  [
    'Côte d’Ivoire',
    'Кот-дИвуар',
    'ci',
    '225'
  ],
  [
    'Croatia (Hrvatska)',
    'Хорватия',
    'hr',
    '385'
  ],
  [
    'Cuba',
    'Куба',
    'cu',
    '53'
  ],
  [
    'Curaçao',
    'Кюрасао',
    'cw',
    '599',
    0
  ],
  [
    'Cyprus (Κύπρος)',
    'Кипр',
    'cy',
    '357'
  ],
  [
    'Czech Republic (Česká republika)',
    'Чехия',
    'cz',
    '420'
  ],
  [
    'Denmark (Danmark)',
    'Дания',
    'dk',
    '45'
  ],
  [
    'Djibouti',
    'Джибути',
    'dj',
    '253'
  ],
  [
    'Dominica',
    'Доминика',
    'dm',
    '1767'
  ],
  [
    'Dominican Republic (República Dominicana)',
    'Доминиканская Республика',
    'do',
    '1',
    2,
    ['809', '829', '849']
  ],
  [
    'Ecuador',
    'Эквадор',
    'ec',
    '593'
  ],
  [
    'Egypt (‫مصر‬‎)',
    'Египет',
    'eg',
    '20'
  ],
  [
    'El Salvador',
    'Сальвадор',
    'sv',
    '503'
  ],
  [
    'Equatorial Guinea (Guinea Ecuatorial)',
    'Экваториальная Гвинея',
    'gq',
    '240'
  ],
  [
    'Eritrea',
    'Эритрея',
    'er',
    '291'
  ],
  [
    'Estonia (Eesti)',
    'Эстония',
    'ee',
    '372'
  ],
  [
    'Ethiopia',
    'Эфиопия',
    'et',
    '251'
  ],
  [
    'Falkland Islands (Islas Malvinas)',
    'Фолклендских',
    'fk',
    '500'
  ],
  [
    'Faroe Islands (Føroyar)',
    'Фарерские Острова',
    'fo',
    '298'
  ],
  [
    'Fiji',
    'Фиджи',
    'fj',
    '679'
  ],
  [
    'Finland (Suomi)',
    'Финляндия',
    'fi',
    '358',
    0
  ],
  [
    'France',
    'Франция',
    'fr',
    '33'
  ],
  [
    'French Guiana (Guyane française)',
    'Французская Гвиана',
    'gf',
    '594'
  ],
  [
    'French Polynesia (Polynésie française)',
    'Французская Полинезия',
    'pf',
    '689'
  ],
  [
    'Gabon',
    'Габон',
    'ga',
    '241'
  ],
  [
    'Gambia',
    'Гамбия',
    'gm',
    '220'
  ],
  [
    'Georgia (საქართველო)',
    'Грузия',
    'ge',
    '995'
  ],
  [
    'Germany (Deutschland)',
    'Германия',
    'de',
    '49'
  ],
  [
    'Ghana (Gaana)',
    'Гана',
    'gh',
    '233'
  ],
  [
    'Gibraltar',
    'Гибралтар',
    'gi',
    '350'
  ],
  [
    'Greece (Ελλάδα)',
    'Греция',
    'gr',
    '30'
  ],
  [
    'Greenland (Kalaallit Nunaat)',
    'Гренландия',
    'gl',
    '299'
  ],
  [
    'Grenada',
    'Гренада',
    'gd',
    '1473'
  ],
  [
    'Guadeloupe',
    'Гваделупа',
    'gp',
    '590',
    0
  ],
  [
    'Guam',
    'Гуам',
    'gu',
    '1671'
  ],
  [
    'Guatemala',
    'Гватемала',
    'gt',
    '502'
  ],
  [
    'Guernsey',
    'Гернси',
    'gg',
    '44',
    1
  ],
  [
    'Guinea (Guinée)',
    'Гвинея',
    'gn',
    '224'
  ],
  [
    'Guinea-Bissau (Guiné Bissau)',
    'Гвинея-Бисау',
    'gw',
    '245'
  ],
  [
    'Guyana',
    'Гайана',
    'gy',
    '592'
  ],
  [
    'Haiti',
    'Гаити',
    'ht',
    '509'
  ],
  [
    'Honduras',
    'Гондурас',
    'hn',
    '504'
  ],
  [
    'Hong Kong (香港)',
    'Гонконг',
    'hk',
    '852'
  ],
  [
    'Hungary (Magyarország)',
    'Венгрия',
    'hu',
    '36'
  ],
  [
    'Iceland (Ísland)',
    'Исландия',
    'is',
    '354'
  ],
  [
    'India (भारत)',
    'Индия',
    'in',
    '91'
  ],
  [
    'Indonesia',
    'Индонезия',
    'id',
    '62'
  ],
  [
    'Iran (‫ایران‬‎)',
    'Иран',
    'ir',
    '98'
  ],
  [
    'Iraq (‫العراق‬‎)',
    'Ирак',
    'iq',
    '964'
  ],
  [
    'Ireland',
    'Ирландия',
    'ie',
    '353'
  ],
  [
    'Isle of Man',
    'Остров Мэн',
    'im',
    '44',
    2
  ],
  [
    'Israel (‫ישראל‬‎)',
    'Израиль',
    'il',
    '972'
  ],
  [
    'Italy (Italia)',
    'Италия',
    'it',
    '39',
    0
  ],
  [
    'Jamaica',
    'Ямайка',
    'jm',
    '1876'
  ],
  [
    'Japan (日本)',
    'Япония',
    'jp',
    '81'
  ],
  [
    'Jersey',
    'Джерси',
    'je',
    '44',
    3
  ],
  [
    'Jordan (‫الأردن‬‎)',
    'Иордания',
    'jo',
    '962'
  ],
  [
    'Kazakhstan (Казахстан)',
    'Казахстан',
    'kz',
    '7',
    1
  ],
  [
    'Kenya',
    'Кения',
    'ke',
    '254'
  ],
  [
    'Kiribati',
    'Кирибати',
    'ki',
    '686'
  ],
  [
    'Kosovo',
    'Косово',
    'xk',
    '383'
  ],
  [
    'Kuwait (‫الكويت‬‎)',
    'Кувейт',
    'kw',
    '965'
  ],
  [
    'Kyrgyzstan (Кыргызстан)',
    'Кыргызстан',
    'kg',
    '996'
  ],
  [
    'Laos (ລາວ)',
    'Лаос',
    'la',
    '856'
  ],
  [
    'Latvia (Latvija)',
    'Латвия',
    'lv',
    '371'
  ],
  [
    'Lebanon (‫لبنان‬‎)',
    'Ливана',
    'lb',
    '961'
  ],
  [
    'Lesotho',
    'Лесото',
    'ls',
    '266'
  ],
  [
    'Liberia',
    'Либерия',
    'lr',
    '231'
  ],
  [
    'Libya (‫ليبيا‬‎)',
    'Ливия',
    'ly',
    '218'
  ],
  [
    'Liechtenstein',
    'Лихтенштейн',
    'li',
    '423'
  ],
  [
    'Lithuania (Lietuva)',
    'Литва',
    'lt',
    '370'
  ],
  [
    'Luxembourg',
    'Люксембург',
    'lu',
    '352'
  ],
  [
    'Macau (澳門)',
    'Макао',
    'mo',
    '853'
  ],
  [
    'Macedonia (FYROM) (Македонија)',
    'Македония',
    'mk',
    '389'
  ],
  [
    'Madagascar (Madagasikara)',
    'Мадагаскар',
    'mg',
    '261'
  ],
  [
    'Malawi',
    'Малави',
    'mw',
    '265'
  ],
  [
    'Malaysia',
    'Малайзия',
    'my',
    '60'
  ],
  [
    'Maldives',
    'Мальдивы',
    'mv',
    '960'
  ],
  [
    'Mali',
    'Мали',
    'ml',
    '223'
  ],
  [
    'Malta',
    'Мальта',
    'mt',
    '356'
  ],
  [
    'Marshall Islands',
    'Маршалловых Островов',
    'mh',
    '692'
  ],
  [
    'Martinique',
    'Мартиника',
    'mq',
    '596'
  ],
  [
    'Mauritania (‫موريتانيا‬‎)',
    'Мавритания',
    'mr',
    '222'
  ],
  [
    'Mauritius (Moris)',
    'Маврикий (Морис)',
    'mu',
    '230'
  ],
  [
    'Mayotte',
    'Майотта',
    'yt',
    '262',
    1
  ],
  [
    'Mexico (México)',
    'Мексика',
    'mx',
    '52'
  ],
  [
    'Micronesia',
    'Микронезия',
    'fm',
    '691'
  ],
  [
    'Moldova (Republica Moldova)',
    'Молдова (Республика Молдова)',
    'md',
    '373'
  ],
  [
    'Monaco',
    'Монако',
    'mc',
    '377'
  ],
  [
    'Mongolia (Монгол)',
    'Монголия (Монгол)',
    'mn',
    '976'
  ],
  [
    'Montenegro (Crna Gora)',
    'Черногория',
    'me',
    '382'
  ],
  [
    'Montserrat',
    'Монтсеррат',
    'ms',
    '1664'
  ],
  [
    'Morocco (‫المغرب‬‎)',
    'Марокко',
    'ma',
    '212',
    0
  ],
  [
    'Mozambique (Moçambique)',
    'Мозамбик',
    'mz',
    '258'
  ],
  [
    'Myanmar (Burma) (မြန်မာ)',
    'Мьянма',
    'mm',
    '95'
  ],
  [
    'Namibia (Namibië)',
    'Намибия',
    'na',
    '264'
  ],
  [
    'Nauru',
    'Науру',
    'nr',
    '674'
  ],
  [
    'Nepal (नेपाल)',
    'Непал',
    'np',
    '977'
  ],
  [
    'Netherlands (Nederland)',
    'идерланды',
    'nl',
    '31'
  ],
  [
    'New Caledonia (Nouvelle-Calédonie)',
    'Новая Каледония',
    'nc',
    '687'
  ],
  [
    'New Zealand',
    'Новая Зеландия',
    'nz',
    '64'
  ],
  [
    'Nicaragua',
    'Никарагуа',
    'ni',
    '505'
  ],
  [
    'Niger (Nijar)',
    'Нигер',
    'ne',
    '227'
  ],
  [
    'Nigeria',
    'Нигерия',
    'ng',
    '234'
  ],
  [
    'Niue',
    'Ниуэ',
    'nu',
    '683'
  ],
  [
    'Norfolk Island',
    'Остров Норфолк',
    'nf',
    '672'
  ],
  [
    'North Korea (조선 민주주의 인민 공화국)',
    'Северная Корея',
    'kp',
    '850'
  ],
  [
    'Northern Mariana Islands',
    'Северные Марианские Острова',
    'mp',
    '1670'
  ],
  [
    'Norway (Norge)',
    'Норвегия',
    'no',
    '47',
    0
  ],
  [
    'Oman (‫عُمان‬‎)',
    'Оман',
    'om',
    '968'
  ],
  [
    'Pakistan (‫پاکستان‬‎)',
    'Пакистан',
    'pk',
    '92'
  ],
  [
    'Palau',
    'Палау',
    'pw',
    '680'
  ],
  [
    'Palestine (‫فلسطين‬‎)',
    'Палестина',
    'ps',
    '970'
  ],
  [
    'Panama (Panamá)',
    'Панама',
    'pa',
    '507'
  ],
  [
    'Papua New Guinea',
    'Папуа-Новая Гвинея',
    'pg',
    '675'
  ],
  [
    'Paraguay',
    'Парагвай',
    'py',
    '595'
  ],
  [
    'Peru (Perú)',
    'Перу',
    'pe',
    '51'
  ],
  [
    'Philippines',
    'Филиппины',
    'ph',
    '63'
  ],
  [
    'Poland (Polska)',
    'Польша',
    'pl',
    '48'
  ],
  [
    'Portugal',
    'Португалия',
    'pt',
    '351'
  ],
  [
    'Puerto Rico',
    'Пуэрто-Рико',
    'pr',
    '1',
    3,
    ['787', '939']
  ],
  [
    'Qatar (‫قطر‬‎)',
    'Катар',
    'qa',
    '974'
  ],
  [
    'Réunion (La Réunion)',
    'Реюньон',
    're',
    '262',
    0
  ],
  [
    'Romania (România)',
    'Румыния',
    'ro',
    '40'
  ],
  [
    'Russia (Россия)',
    'Россия',
    'ru',
    '7',
    0
  ],
  [
    'Rwanda',
    'Руанда',
    'rw',
    '250'
  ],
  [
    'Saint Barthélemy',
    'Сен-Бартелеми',
    'bl',
    '590',
    1
  ],
  [
    'Saint Helena',
    'Остров Святой Елены',
    'sh',
    '290'
  ],
  [
    'Saint Kitts and Nevis',
    'Сент-Китс и Невис',
    'kn',
    '1869'
  ],
  [
    'Saint Lucia',
    'Сент-Люсия',
    'lc',
    '1758'
  ],
  [
    'Saint Martin (Saint-Martin (partie française))',
    'Сен-Мартен',
    'mf',
    '590',
    2
  ],
  [
    'Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)',
    'Сен-Пьер и Микелон',
    'pm',
    '508'
  ],
  [
    'Saint Vincent and the Grenadines',
    'Сент-Винсент и Гренадины',
    'vc',
    '1784'
  ],
  [
    'Samoa',
    'Самоа',
    'ws',
    '685'
  ],
  [
    'San Marino',
    'Сан-Марино',
    'sm',
    '378'
  ],
  [
    'São Tomé and Príncipe (São Tomé e Príncipe)',
    'Сан-Томе и Принсипи',
    'st',
    '239'
  ],
  [
    'Saudi Arabia (‫المملكة العربية السعودية‬‎)',
    'Саудовская Аравия',
    'sa',
    '966'
  ],
  [
    'Senegal (Sénégal)',
    'Сенегал',
    'sn',
    '221'
  ],
  [
    'Serbia (Србија)',
    'Сербия',
    'rs',
    '381'
  ],
  [
    'Seychelles',
    'Сейшелы',
    'sc',
    '248'
  ],
  [
    'Sierra Leone',
    'Сьерра-Леоне',
    'sl',
    '232'
  ],
  [
    'Singapore',
    'Сингапур',
    'sg',
    '65'
  ],
  [
    'Sint Maarten',
    'Синт-Мартен',
    'sx',
    '1721'
  ],
  [
    'Slovakia (Slovensko)',
    'Словакии',
    'sk',
    '421'
  ],
  [
    'Slovenia (Slovenija)',
    'Словения',
    'si',
    '386'
  ],
  [
    'Solomon Islands',
    'Соломоновы Острова',
    'sb',
    '677'
  ],
  [
    'Somalia (Soomaaliya)',
    'Сомали',
    'so',
    '252'
  ],
  [
    'South Africa',
    'Южная Африка',
    'za',
    '27'
  ],
  [
    'South Korea (대한민국)',
    'Южная Корея',
    'kr',
    '82'
  ],
  [
    'South Sudan (‫جنوب السودان‬‎)',
    'Южный Судан',
    'ss',
    '211'
  ],
  [
    'Spain (España)',
    'Испания',
    'es',
    '34'
  ],
  [
    'Sri Lanka (ශ්‍රී ලංකාව)',
    'Шри-Ланка',
    'lk',
    '94'
  ],
  [
    'Sudan (‫السودان‬‎)',
    'Судан',
    'sd',
    '249'
  ],
  [
    'Suriname',
    'Суринам',
    'sr',
    '597'
  ],
  [
    'Svalbard and Jan Mayen',
    'Шпицберген и Ян-Майен',
    'sj',
    '47',
    1
  ],
  [
    'Swaziland',
    'Свазиленд',
    'sz',
    '268'
  ],
  [
    'Sweden (Sverige)',
    'Швеция',
    'se',
    '46'
  ],
  [
    'Switzerland (Schweiz)',
    'Швейцария',
    'ch',
    '41'
  ],
  [
    'Syria (‫سوريا‬‎)',
    'Сирия',
    'sy',
    '963'
  ],
  [
    'Taiwan (台灣)',
    'Тайвань',
    'tw',
    '886'
  ],
  [
    'Tajikistan',
    'Таджикистан',
    'tj',
    '992'
  ],
  [
    'Tanzania',
    'Танзания',
    'tz',
    '255'
  ],
  [
    'Thailand (ไทย)',
    'Таиланд',
    'th',
    '66'
  ],
  [
    'Timor-Leste',
    'Тимор-Лешти',
    'tl',
    '670'
  ],
  [
    'Togo',
    'Того',
    'tg',
    '228'
  ],
  [
    'Tokelau',
    'Токелау',
    'tk',
    '690'
  ],
  [
    'Tonga',
    'Тонга',
    'to',
    '676'
  ],
  [
    'Trinidad and Tobago',
    'Тринидад и Тобаго',
    'tt',
    '1868'
  ],
  [
    'Tunisia (‫تونس‬‎)',
    'Тунис',
    'tn',
    '216'
  ],
  [
    'Turkey (Türkiye)',
    'Турция',
    'tr',
    '90'
  ],
  [
    'Turkmenistan',
    'Туркменистан',
    'tm',
    '993'
  ],
  [
    'Turks and Caicos Islands',
    'Острова Теркс и Кайкос',
    'tc',
    '1649'
  ],
  [
    'Tuvalu',
    'Тувалу',
    'tv',
    '688'
  ],
  [
    'U.S. Virgin Islands',
    'Американские Виргинские острова',
    'vi',
    '1340'
  ],
  [
    'Uganda',
    'Уганда',
    'ug',
    '256'
  ],
  [
    'Ukraine (Україна)',
    'Украина',
    'ua',
    '380'
  ],
  [
    'United Arab Emirates (‫الإمارات العربية المتحدة‬‎)',
    'Объединенные Арабские Эмираты',
    'ae',
    '971'
  ],
  [
    'United Kingdom',
    'Великобритания',
    'gb',
    '44',
    0
  ],
  [
    'United States',
    'Соединенные Штаты',
    'us',
    '1',
    0
  ],
  [
    'Uruguay',
    'Уругвай',
    'uy',
    '598'
  ],
  [
    'Uzbekistan (Oʻzbekiston)',
    'Узбекистан',
    'uz',
    '998'
  ],
  [
    'Vanuatu',
    'Вануату',
    'vu',
    '678'
  ],
  [
    'Vatican City (Città del Vaticano)',
    'Вирджиния',
    'va',
    '39',
    1
  ],
  [
    'Venezuela',
    'Венесуэла',
    've',
    '58'
  ],
  [
    'Vietnam (Việt Nam)',
    'Вьетнам',
    'vn',
    '84'
  ],
  [
    'Wallis and Futuna (Wallis-et-Futuna)',
    'Уоллис и Футуна',
    'wf',
    '681'
  ],
  [
    'Western Sahara (‫الصحراء الغربية‬‎)',
    'Западной Сахаре',
    'eh',
    '212',
    1
  ],
  [
    'Yemen (‫اليمن‬‎)',
    'Йемен',
    'ye',
    '967'
  ],
  [
    'Zambia',
    'Замбия',
    'zm',
    '260'
  ],
  [
    'Zimbabwe',
    'Зимбабве',
    'zw',
    '263'
  ],
  [
    'Åland Islands',
    'Аландские Острова',
    'ax',
    '358',
    1
  ]
]

export default allCountries.map(country => ({
  name: country[0],
  nameRu: country[1],
  iso2: country[2].toUpperCase(),
  dialCode: country[3],
  priority: country[4] || 1,
  areaCodes: country[5] || null
}))
