export { default as IconBase } from './IconBase.vue'
export { default as Question } from './Question.vue'
export { default as Language } from './Language.vue'
export { default as Social } from './Social.vue'
export { default as InputCustom } from './InputCustom.vue'
export { default as InputPassCustom } from './InputPassCustom.vue'
export { default as LabelLCustom } from './LabelLCustom.vue'

export { default as Doctors } from './sliders/Doctors.vue'

export { default as Ask } from './forms/Ask.vue'
export { default as Feedback } from './forms/Feedback.vue'
