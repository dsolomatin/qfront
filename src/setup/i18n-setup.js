import Vue from 'vue'
import VueI18n from 'vue-i18n'
// import { enLanguage } from '../lang/en'
import { ruLanguage } from '../lang/ru'
import axios from 'axios'
import fetchService from '../services'

Vue.use(VueI18n)

export const i18n = new VueI18n({
  locale: 'ru', // set locale
  fallbackLocale: 'ru',
  messages: ruLanguage // set locale messages
})

const loadedLanguages = ['ru'] // our default language that is preloaded

function setI18nLanguage (lang) {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

export function loadLanguageAsync (lang) {
  // If the same language
  if (i18n.locale === lang) {
    return Promise.resolve(setI18nLanguage(lang))
  }

  // If the language was already loaded
  if (loadedLanguages.includes(lang)) {
    return Promise.resolve(setI18nLanguage(lang))
  }

  // If the language hasn't been loaded yet
  return fetchService.getLanguage(`${lang}.json`)
    // .then(messages => messages.json())
    .then(
      messages => {
        i18n.setLocaleMessage(lang, messages)
        loadedLanguages.push(lang)
        return setI18nLanguage(lang)
      }
    )
}
