// This module is split for every folder, this should contain a all text of application
// Every variable correspond to the class-name
// Path var 'component.component_name.class-name.description'
export const ruLanguage = {
  'ru': {
    'components': {
      'questions': {
        'formTitle': {
          'ask': 'Спросите что угодно',
          'from': 'у',
          'time': 'наших врачей',
          'sub': 'Ваш вопрос будет отправлен всем специалистам в данной области',
          'questionMark': 'Перейдите в ',
          'questionMark_2': 'личный кабинет ',
          'questionMark_3': 'для выбора специалиста'
        },
        'formSubtitle': {
          'ask': 'Ваш вопрос будет отправлен всем специалистам в данной области',
          'auth': 'Авторизуйтесь',
          'field': 'для выбора конкретного специалиста'
        },
        'bForm': {
          'characters': 'Осталось 150 символов...',
          'send': 'Отправить'
        }
      },
      'slider': {}
    },
    'layouts': {
      'footer': {
        'name': 'Капсула',
        'allRightsReserved': 'Все права защищены.',
        'about': 'О нас',
        'company': 'Компания',
        'contact': 'Контакты',
        'terms': 'Условия использования',
        'confidential': 'Конфиденциальность',
        'patient': 'Пациентам',
        'opportunities': 'Возможности',
        'ask': 'Задать вопрос врачу',
        'title': 'Контроль диабета',
        'check': 'Акку-Чек',
        'from': 'с',
        'doctor': 'Врачам',
        'consult': 'Стать консультантом',
        'feedback': 'Обратная связь'
      },
      'header': {
        'menu': {
          'base': 'База знаний',
          'about': 'О нас',
          'patients': 'Пациентам',
          'doctors': 'Врачам'
        },
        'loginBtn': 'Вход | Регистрация'
      }
    },
    'pages': {
      '404': {},
      'about': {},
      'agreement': {},
      'ask': {},
      'base': {},
      'confidential': {},
      'develops': {},
      'doctors': {},
      'faq': {},
      'login': {},
      'patients': {}
    }
  }
}
