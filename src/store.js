import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    // Varibles
  },
  mutations: {
    // Functions, it should be return a state var
  }
})

export default store
