import '@babel/polyfill'
import 'mutationobserver-shim'
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App'
import router from './router'
// import {i18n, loadLanguageAsync} from './setup/i18n-setup'
import { i18n } from './setup/i18n-setup'
import VueFuse from 'vue-fuse'
import VModal from 'vue-js-modal'

import '../node_modules/slick-carousel/slick/slick.css'
import './assets/fonts/fonts.css'

// Vue.use(BootstrapVue)
Vue.use(VueFuse)
Vue.use(VModal)

Vue.config.productionTip = false

/* eslint-disable no-new */

new Vue({
  el: '#app',
  router,
  i18n,
  components: { App },
  template: '<App/>'
})
